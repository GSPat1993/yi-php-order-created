<?php 
	require './vendor/autoload.php';
	require_once 'functions.php';

	$bringgData = file_get_contents('php://input');
	$data = json_decode($bringgData);

	/* YIHAWKER WEBSITE */
	const DEFAULT_URL = 'https://yihawker-website.firebaseio.com';
  const DEFAULT_TOKEN = 'vKIBdp1GQ69ixww4pmG9kHu4V9sNp7AiiAj0pkAt';
  const DEFAULT_PATH = '/';

	/* YIHAWKER DEV */
	// const DEFAULT_URL = 'https://yihawker-dev.firebaseio.com';
  // const DEFAULT_TOKEN = '286bCKjlt8WPMvDK9Y8nvFee40qrDsAX2oB8krZr';
  // const DEFAULT_PATH = '/';

	$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
	$url = "https://beta.yihawker.today/accepted-by-driver/";


	if(contains("ORDER-YI",$data->title)){
		// $status = array('status' => 10);
		// $ordersPath = "/orders/". $data->id;
		// $ordersPerCustomerPath = "/orders_per_customer/". $data->customer_id."/".$data->id;
		// $orderPerStall = "/orders_per_stall/". $data->customer_id."/".$data->id;
		// updateFirebase($firebase,$ordersPath,$status);
		// updateFirebase($firebase,$ordersPerCustomerPath,$status);
		// updateFirebase($firebase,$path,$status);
		// updateFirebase($firebase,$path,$status);

		$bringgID = $data->id;
		$path = '/orders/';
		$time = time();
		$data = array('date_created' => $time);
		$update = array('status' => 10);

		$value = $firebase->get($path, array('orderBy' => '"bringg_id"', 'equalTo' => '"'.$bringgID.'"'));
		$newValue = json_decode($value, true);
		$key = key($newValue);

		$customerId = $newValue[$key]['customer_id'];
		$stallId = $newValue[$key]['stall_id'];

		$orderPath = $path.$key;
		$orderPerCustomerPath = "/orders_per_customer/".$customerId."/".$key;
		$orderPerStallPath = "/orders_per_stall/".$stallId."/".$key;
		
		$firebase->update($orderPerCustomerPath, $data);
		$firebase->update($orderPerStallPath, $data);
		$firebase->update($orderPath, $data);

		$firebase->update($orderPerCustomerPath, $update);
		$firebase->update($orderPerStallPath, $update);
		$firebase->update($orderPath, $update);

	}else{
		sendPostData($url,$bringgData);
	}


?>